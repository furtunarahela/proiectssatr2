/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

/**
 *
 * @author Rahela
 */
public class Animal implements WeatherObserver {

    private Boolean isVegetarian;
    private String food;
    private Integer nrLegs;
    private Region region;

    public Animal(Boolean isVegetarian, String food, Integer nrLegs, Region region) {
        this.isVegetarian = isVegetarian;
        this.food = food;
        this.nrLegs = nrLegs;
        this.region = region;
    }

    public Boolean getIsVegetarian() {
        return isVegetarian;
    }

    public String getFood() {
        return food;
    }

    public Integer getNrLegs() {
        return nrLegs;
    }

    public void setIsVegetarian(Boolean isVegetarian) {
        this.isVegetarian = isVegetarian;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public void setNrLegs(Integer nrLegs) {
        this.nrLegs = nrLegs;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Region getRegion() {
        return region;
    }
    
    public String display(){
        return "Animal -> ";
    }

    @Override
    public void update(WeatherType currentWeather) {
        switch (currentWeather) {
            case COLD:
                System.out.println("The animals are freezing cold.");
                break;
            case RAINY:
                System.out.println("The animals are dripping wet.");
                break;
            case SUNNY:
                System.out.println("The animals are happy.");
                break;
            case WINDY:
                System.out.println("The animals are searching for shelter.");
                break;
            default:
                break;
        }
    }

}
