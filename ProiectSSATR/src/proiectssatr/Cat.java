/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

/**
 *
 * @author Rahela
 */
public class Cat extends Animal{
    private String color;
    
    public Cat(Boolean isVegetarian, String food, Integer nrLegs, Region region, String color) {
        super(isVegetarian, food, nrLegs,region);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String display(){
        return super.display() + "Cat with " + this.color + " color";
    }

    @Override
    public void update(WeatherType currentWeather) {
        switch (currentWeather) {
            case COLD:
                System.out.println("The cats are freezing cold.");
                break;
            case RAINY:
                System.out.println("The cats are dripping wet.");
                break;
            case SUNNY:
                System.out.println("The cats are happy.");
                break;
            case WINDY:
                System.out.println("The cats are searching for shelter.");
                break;
            default:
                break;
        }
    }
    
    
    
}
