/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

/**
 *
 * @author Rahela
 */
public enum WeatherType {
   SUNNY, RAINY, WINDY, COLD;  
   
  @Override
  public String toString() {
    return this.name().toLowerCase();
  }
}
