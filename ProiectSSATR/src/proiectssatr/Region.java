/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

/**
 *
 * @author Rahela
 */
public class Region {
    private String name;
    private String country;

    public Region(String name, String country) {
        this.name = name;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    public String displayRegion(){
        return "Region: " + this.name + " " + this.country;
    }
}
