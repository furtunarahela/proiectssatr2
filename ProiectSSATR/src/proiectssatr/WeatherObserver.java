/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

/**
 *
 * @author Rahela
 */
public interface WeatherObserver {
    void update(WeatherType currentWeather);
}