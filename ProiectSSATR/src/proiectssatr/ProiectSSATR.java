/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Rahela
 */
public class ProiectSSATR {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        List<Human> humans = generateHumanList();
        List<Animal> animals = generateAnimalList();

        Scanner scanner = new Scanner(System.in);
        int choice = 9;

        do {
            System.out.println("-------------------------\n");
            System.out.println("Choose from these choices");
            System.out.println("-------------------------\n");
            System.out.println("1 - Humans list");
            System.out.println("2 - Cats list");
            System.out.println("3 - Animals by region");
            System.out.println("4 - Try different weathers");
            System.out.println("5 - Change weather");

            choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    getHumans(humans);
                    break;
                case 2:
                    getCats(animals);
                    break;
                case 3:
                    Scanner scann = new Scanner(System.in);
                    String region = scann.next();
                    getAnimalsPerRegion(animals, region);
                    break;
                case 4:
                    changeAllWeather();
                    break;
                case 5:
                    System.out.println("-------------------------\n");
                    System.out.println("Choose from these choices");
                    System.out.println("-------------------------\n");
                    System.out.println("0 - SUNNY");
                    System.out.println("1 - RAINY");
                    System.out.println("2 - WINDY");
                    System.out.println("3 - COLD");

                    Scanner scann2 = new Scanner(System.in);
                    int weather = scann2.nextInt();
                    changeWeather(weather);
                default:
                // The user input an unexpected choice.

            }
        } while (choice != 0);
    }

    private static List<Human> generateHumanList() {
        List<Human> humans = new ArrayList<>();

        Region cluj = new Region("Cluj", "Romania");
        Region sebes = new Region("Sebes", "Romania");
        Region oradea = new Region("Oradea", "Romania");
        Region zalau = new Region("Zalau", "Romania");

        Human h1 = new Human("F", "Maria", "21", zalau);
        humans.add(h1);
        Human h2 = new Human("F", "Ana", "41", oradea);
        humans.add(h2);
        Human h3 = new Human("F", "Mirabela", "67", zalau);
        humans.add(h3);
        Human h4 = new Human("M", "Ion", "29", sebes);
        humans.add(h4);
        Human h5 = new Human("M", "Vasile", "31", cluj);
        humans.add(h5);

        return humans;
    }

    private static void getHumans(List<Human> humans) {
        for (Human human : humans) {
            String message = human.getName() + " -> " + human.getRegion().displayRegion();
            System.out.println(message);
        }
    }

    private static List<Animal> generateAnimalList() {
        Region cluj = new Region("Cluj", "Romania");
        Region sebes = new Region("Sebes", "Romania");
        Region oradea = new Region("Oradea", "Romania");
        Region zalau = new Region("Zalau", "Romania");

        List<Animal> animals = new ArrayList<>();

        Animal animal1 = new Cat(false, "fish", 4, cluj, "black");
        animals.add(animal1);
        Animal animal2 = new Cat(false, "fish", 4, sebes, "white");
        animals.add(animal2);
        Animal animal3 = new Cat(false, "fish", 4, oradea, "grey");
        animals.add(animal3);
        Animal animal4 = new Cat(false, "fish", 4, cluj, "black");
        animals.add(animal4);
        Animal animal5 = new Cat(false, "fish", 4, cluj, "grey");
        animals.add(animal5);
        Animal animal6 = new Cat(false, "fish", 4, zalau, "orange");
        animals.add(animal6);
        Animal animal7 = new Dog(false, "fish", 4, zalau, "pomeranian");
        animals.add(animal7);
        Animal animal8 = new Dog(false, "fish", 4, cluj, "husky");
        animals.add(animal8);
        Animal animal9 = new Dog(false, "fish", 4, oradea, "labrador");
        animals.add(animal9);
        Animal animal10 = new Dog(false, "fish", 4, sebes, "pudel");
        animals.add(animal10);

        return animals;
    }

    private static void getCats(List<Animal> animals) {
        for (Animal animal : animals) {
            if (animal instanceof Cat) {
                String message = "This cat is from " + animal.getRegion().getName() + ", has color " + ((Cat) animal).getColor() + " and eats " + animal.getFood();
                System.out.println(message);
            }
        }
    }

    private static void getAnimalsPerRegion(List<Animal> animals, String region) {

        String message = "Animals in " + region + ": ";
        System.out.println(message);
        for (Animal animal : animals) {
            if (animal.getRegion().getName().equals(region)) {
                System.out.println(animal.display());
            }
        }

    }

    private static void changeAllWeather() {

        Region cluj = new Region("Cluj", "Romania");

        Weather weather = new Weather();
        weather.addObserver(new Human("M", "Vasile", "31", cluj));
        weather.addObserver(new Cat(false, "fish", 4, cluj, "black"));
        weather.addObserver(new Dog(false, "fish", 4, cluj, "labrador"));

        weather.changeWeather();
        weather.changeWeather();
        weather.changeWeather();
        weather.changeWeather();

    }

    private static void changeWeather(int weather) {
        Region cluj = new Region("Cluj", "Romania");

        Weather weather2 = new Weather();
        weather2.addObserver(new Human("M", "Vasile", "31", cluj));
        weather2.addObserver(new Cat(false, "fish", 4, cluj, "black"));
        weather2.addObserver(new Dog(false, "fish", 4, cluj, "labrador"));
        
        WeatherType[] enumValues = WeatherType.values();
        WeatherType currentWeather = enumValues[weather];
        
        weather2.setWeather(currentWeather);
    }

}
