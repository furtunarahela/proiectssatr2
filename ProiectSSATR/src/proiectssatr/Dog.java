/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

/**
 *
 * @author Rahela
 */
public class Dog extends Animal {
    private String breed;

    public Dog(Boolean isVegetarian, String food, Integer nrLegs, Region region, String breed) {
        super(isVegetarian, food, nrLegs, region);
        this.breed = breed;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    @Override
    public String display() {
        return super.display() + "Dog from breed " + this.breed;
    }
    
    

    @Override
    public void update(WeatherType currentWeather) {
       switch (currentWeather) {
            case COLD:
                System.out.println("The dogs are freezing cold.");
                break;
            case RAINY:
                System.out.println("The dogs are dripping wet.");
                break;
            case SUNNY:
                System.out.println("The dogs are happy.");
                break;
            case WINDY:
                System.out.println("The dogs are searching for shelter.");
                break;
            default:
                break;
        }
    }
    
    
    
}
