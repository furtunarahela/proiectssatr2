/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

/**
 *
 * @author Rahela
 */
public class Human implements WeatherObserver {

    private String sex;
    private String name;
    private String age;
    private Region region;

    public Human(String sex, String name, String age, Region region) {
        this.sex = sex;
        this.name = name;
        this.age = age;
        this.region = region;
    }

    public String getSex() {
        return sex;
    }

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }
    

    @Override
    public void update(WeatherType currentWeather) {
        switch (currentWeather) {
            case COLD:
                System.out.println("Humans are shivering in the cold weather.");
                break;
            case RAINY:
                System.out.println("Humans look for cover from the rain.");
                break;
            case SUNNY:
                System.out.println("Humans are happy in the warm sun.");
                break;
            case WINDY:
                System.out.println("Humans hold their hats tightly in the windy weather.");
                break;
            default:
                break;
        }
    }

}
