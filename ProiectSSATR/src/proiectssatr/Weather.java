/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proiectssatr;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Rahela
 */
public class Weather {
    
  private WeatherType currentWeather;
  private List<WeatherObserver> observers;

  public Weather() {
    observers = new ArrayList<>();
    currentWeather = WeatherType.SUNNY;
  }

  public void addObserver(WeatherObserver obs) {
    observers.add(obs);
  }

  public void removeObserver(WeatherObserver obs) {
    observers.remove(obs);
  }

  /**
   * Makes time pass for weather.
   */
  public void changeWeather() {
    WeatherType[] enumValues = WeatherType.values();
    currentWeather = enumValues[(currentWeather.ordinal() + 1) % enumValues.length];
    String message = "The weather changed to " + currentWeather;
    System.out.println(message);
    notifyObservers();
  }
  
  public void setWeather(WeatherType weather){
      currentWeather = weather;
      String message = "The weather changed to " + currentWeather;
      System.out.println(message);
      notifyObservers();
  }
          
  private void notifyObservers() {
    for (WeatherObserver obs : observers) {
      obs.update(currentWeather);
    }
  }
}